import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  calc = '';

  constructor() {}

digits = ['1','2','3','4','5','6','7','8','9']
operators = ['+','-',"*","/",'%'];
result = '';

opdateCalc(val:any){
   if(this.operators.includes(val) && this.calc === '' ||
     this.operators.includes(val) && this.operators.includes(this.calc.slice(-1))
    ){
      return;
    }
    this.calc += val;  
    
    if(!this.operators.includes(val)){
      this.result = eval(this.calc);

    }
  
}
calculate(){
  this.calc = eval(this.calc);
}
deleteval(){
  if(this.calc === ''){
    return;
  }

  this.calc = this.calc.substring(0,this.calc.length -1)
}
resetval(){
  this.calc = '';
  this.result = '';
}
}
  